/********************************************************************/
/* Author  : Islam A.                                               */
/* Date    : 21 May 2019                                            */
/* Version : V01                                                    */
/* Description: Configuration file for LED interface                */
/********************************************************************/
/*PreProccessor Guard*/
#ifndef LED_CONFIG_H_
#define LED_CONFIG_H_

/* Initial pins connected all leds							 	 */
/* knowing that the max number of leds can be connected to       */
/* the AVR pins  is 31   										 */
#define LED0_u8_DIO_CH       DIO_u8_PIN0
#define LED1_u8_DIO_CH       DIO_u8_PIN1

/* Initial direction for pins connected all leds		  */
/* Range: LED_u8_FORWARD 	   	   				          */
/*        LED_u8_REVERSE	 	                          */
#define LED0_u8_TYPE         LED_u8_FORWARD
#define LED1_u8_TYPE         LED_u8_FORWARD

/* the number of leds connected to the controller */
#define LED_u8_NUM_OF_LEDS 2

/*Array collecting the pins connected to each configurated led*/
u8 LED_Au8LedCh[LED_u8_NUM_OF_LEDS] = {LED0_u8_DIO_CH,LED1_u8_DIO_CH};

/*Array collecting the connectivity of each led whether it is connected forward (active high) or reverse (active low)*/
u8 LED_Au8LedType[LED_u8_NUM_OF_LEDS] = {LED0_u8_TYPE,LED1_u8_TYPE};

#endif /* LED_CONFIG_H_ */
