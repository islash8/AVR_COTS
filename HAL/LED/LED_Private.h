/********************************************************************/
/* Author  : Islam A.                                               */
/* Date    : 21 May 2019                                            */
/* Version : V01                                                    */
/********************************************************************/
#ifndef LED_PRIV_H_
#define LED_PRIV_H_

/* Connection of LED whether it's forward or reverse */
#define LED_u8_FORWARD (u8)1
#define LED_u8_REVERSE (u8)0

/* Value of LED, whether it's high or low (power on or off) */
#define LED_u8_HIGH   (u8)5
#define LED_u8_LOW 	  (u8)55

#endif /* LED_PRIV_H_ */
