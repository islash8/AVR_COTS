/********************************************************************/
/* Author  : Islam A.                                               */
/* Date    : 21 MAY 2019                                            */
/* Version : V01                                                    */
/* Description: header file for LED interface                       */
/********************************************************************/

/*PreProccessor Guard*/
#ifndef LED_H_INTERFACE_
#define LED_H_INTERFACE_

/* Numbers of LEDs used */
#typedef enum 
{
	LED_u8_No1,
	LED_u8_No2,
}LED_Number;


/*************************************************************************************/
/* Description: Function to power on the LED according to its LED number             */
/* Inputs: the pin number connected to the led										 */
/* output: the Error state of the function											 */
/*************************************************************************************/
u8 LED_u8LedOn (u8 Copy_u8LedNb);

/*************************************************************************************/
/* Description: Function to power on the LED according to its LED number             */
/* Inputs: the pin number connected to the led										 */
/* output: the Error state of the function											 */
/*************************************************************************************/
u8 LED_u8LedOff (u8 Copy_u8LedNb);

#endif /* LED_H_INTERFACE_ */