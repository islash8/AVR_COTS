## LED Driver used for ATmega32 AVR MCU

### LED Interface header file:
1. LED number
2. LED power on function
3. LED power off function

### LED private header file:
1. Macro definition of type of connection
2. Macro definition of value of connection

### LED config header file:
1. Initial values of LEDs
2. Initial values of connections
3. Number of LEDs connected

### LED program source file:
1. Function implementation for powering on LED
2. Function implementation for powering off LED