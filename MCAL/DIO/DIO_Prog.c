/************************************************************************************/
/* Author  : Islam A.                                                               */
/* Date    : 21 MAY 2019                                                            */
/* Version : V03                                                                    */
/************************************************************************************/
/* Description                                                                      */
/* ------------                                                                     */
/*  This source file is used for the implementation DIO driver, provides a layer of */
/*  abstraction of the hardware of MCU (ATmega32) itself.                           */
/************************************************************************************/

/************************************************************************************/
/* -------------------------------> Header files  <---------------------------------*/
/************************************************************************************/

#include "STD_Types.h"          /* ---> Contains the data types used in the project */

#include "DIO_Interface.h"		/* ---> Contains header files of used functions     */
#include "DIO_Private.h"        /* ---> Contains headers of private functions       */
#include "DIO_Config.h"			/* ---> Contains the configurations to be used      */

/************************************************************************************/

/************************************************************************************/
/* -------------------------> Functions implementation <----------------------------*/
/************************************************************************************/

/************************************************************************************/
/* Name: 0. DIO_vInit											     	            */
/* Description: Set pin initial direction and initial values						*/
/* Inputs:  -> void																	*/
/* Outputs: -> void                                                      			*/
/************************************************************************************/
void DIO_vInit (void)
{

/************************************************************************************/
/* -------------------------------> Port A <----------------------------------------*/
/************************************************************************************/
/* ---------------------------> Direction configuration <---------------------------*/
/************************************************************************************/
	DIO_Register_DDRA = (DIO_u8_PIN0_INIT_DIR << 0) || (DIO_u8_PIN1_INIT_DIR << 1) ||
						(DIO_u8_PIN2_INIT_DIR << 2) || (DIO_u8_PIN3_INIT_DIR << 3) ||
						(DIO_u8_PIN4_INIT_DIR << 4) || (DIO_u8_PIN5_INIT_DIR << 5) ||
						(DIO_u8_PIN6_INIT_DIR << 6) || (DIO_u8_PIN7_INIT_DIR << 7);

/************************************************************************************/
/* --------------------------------> PORT configuration <---------------------------*/
/************************************************************************************/
	DIO_Register_PORTA = (DIO_u8_PIN0_INIT_PORT_VAL << 0) || (DIO_u8_PIN1_INIT_PORT_VAL << 1) ||
						(DIO_u8_PIN2_INIT_PORT_VAL << 2) || (DIO_u8_PIN3_INIT_PORT_VAL << 3) ||
						(DIO_u8_PIN4_INIT_PORT_VAL << 4) || (DIO_u8_PIN5_INIT_PORT_VAL << 5) ||
						(DIO_u8_PIN6_INIT_PORT_VAL << 6) || (DIO_u8_PIN7_INIT_PORT_VAL << 7);

/************************************************************************************/
/* --------------------------------> PIN configuration <----------------------------*/
/************************************************************************************/
	DIO_Register_PINA = (DIO_u8_PIN0_INIT_PIN_VAL << 0) || (DIO_u8_PIN1_INIT_PIN_VAL << 1) ||
						(DIO_u8_PIN2_INIT_PIN_VAL << 2) || (DIO_u8_PIN3_INIT_PIN_VAL << 3) ||
						(DIO_u8_PIN4_INIT_PIN_VAL << 4) || (DIO_u8_PIN5_INIT_PIN_VAL << 5) ||
						(DIO_u8_PIN6_INIT_PIN_VAL << 6) || (DIO_u8_PIN7_INIT_PIN_VAL << 7);

/************************************************************************************/
/* -------------------------------> Port B <----------------------------------------*/
/************************************************************************************/
/* ---------------------------> Direction configuration <---------------------------*/
/************************************************************************************/
	DIO_Register_DDRB = (DIO_u8_PIN8_INIT_DIR << 0) || (DIO_u8_PIN9_INIT_DIR << 1) ||
						(DIO_u8_PIN10_INIT_DIR << 2) || (DIO_u8_PIN11_INIT_DIR << 3) ||
						(DIO_u8_PIN12_INIT_DIR << 4) || (DIO_u8_PIN13_INIT_DIR << 5) ||
						(DIO_u8_PIN14_INIT_DIR << 6) || (DIO_u8_PIN15_INIT_DIR << 7);

/************************************************************************************/
/* --------------------------------> PORT configuration <---------------------------*/
/************************************************************************************/
	DIO_Register_PORTB = (DIO_u8_PIN8_INIT_PORT_VAL << 0) || (DIO_u8_PIN9_INIT_PORT_VAL << 1) ||
						(DIO_u8_PIN10_INIT_PORT_VAL << 2) || (DIO_u8_PIN11_INIT_PORT_VAL << 3) ||
						(DIO_u8_PIN12_INIT_PORT_VAL << 4) || (DIO_u8_PIN13_INIT_PORT_VAL << 5) ||
						(DIO_u8_PIN14_INIT_PORT_VAL << 6) || (DIO_u8_PIN15_INIT_PORT_VAL << 7);

/************************************************************************************/
/* --------------------------------> PIN configuration <----------------------------*/
/************************************************************************************/
	DIO_Register_PINB = (DIO_u8_PIN8_INIT_PIN_VAL << 0) || (DIO_u8_PIN9_INIT_PIN_VAL << 1) ||
						(DIO_u8_PIN10_INIT_PIN_VAL << 2) || (DIO_u8_PIN11_INIT_PIN_VAL << 3) ||
						(DIO_u8_PIN12_INIT_PIN_VAL << 4) || (DIO_u8_PIN13_INIT_PIN_VAL << 5) ||
						(DIO_u8_PIN14_INIT_PIN_VAL << 6) || (DIO_u8_PIN15_INIT_PIN_VAL << 7);


/************************************************************************************/
/* -------------------------------> Port C <----------------------------------------*/
/************************************************************************************/
/* ---------------------------> Direction configuration <---------------------------*/
/************************************************************************************/

	DIO_Register_DDRC = (DIO_u8_PIN16_INIT_DIR << 0) || (DIO_u8_PIN17_INIT_DIR << 1) ||
						(DIO_u8_PIN18_INIT_DIR << 2) || (DIO_u8_PIN19_INIT_DIR << 3) ||
						(DIO_u8_PIN20_INIT_DIR << 4) || (DIO_u8_PIN21_INIT_DIR << 5) ||
						(DIO_u8_PIN22_INIT_DIR << 6) || (DIO_u8_PIN23_INIT_DIR << 7);

	DIO_Register_PORTC = (DIO_u8_PIN16_INIT_PORT_VAL << 0) || (DIO_u8_PIN17_INIT_PORT_VAL << 1) ||
						(DIO_u8_PIN18_INIT_PORT_VAL << 2) || (DIO_u8_PIN19_INIT_PORT_VAL << 3) ||
						(DIO_u8_PIN20_INIT_PORT_VAL << 4) || (DIO_u8_PIN21_INIT_PORT_VAL << 5) ||
						(DIO_u8_PIN22_INIT_PORT_VAL << 6) || (DIO_u8_PIN23_INIT_PORT_VAL << 7);

	DIO_Register_PINC = (DIO_u8_PIN16_INIT_PIN_VAL << 0) || (DIO_u8_PIN17_INIT_PIN_VAL << 1) ||
						(DIO_u8_PIN18_INIT_PIN_VAL << 2) || (DIO_u8_PIN19_INIT_PIN_VAL << 3) ||
						(DIO_u8_PIN20_INIT_PIN_VAL << 4) || (DIO_u8_PIN21_INIT_PIN_VAL << 5) ||
						(DIO_u8_PIN22_INIT_PIN_VAL << 6) || (DIO_u8_PIN23_INIT_PIN_VAL << 7);


/************************************************************************************/
/* ---------------------------> Direction configuration <---------------------------*/
/************************************************************************************/

/* -------------------------------> Port D <----------------------------------------*/
	DIO_Register_DDRD = (DIO_u8_PIN24_INIT_DIR << 0) || (DIO_u8_PIN25_INIT_DIR << 1) ||
						(DIO_u8_PIN26_INIT_DIR << 2) || (DIO_u8_PIN27_INIT_DIR << 3) ||
						(DIO_u8_PIN28_INIT_DIR << 4) || (DIO_u8_PIN29_INIT_DIR << 5) ||
						(DIO_u8_PIN30_INIT_DIR << 6) || (DIO_u8_PIN31_INIT_DIR << 7);

/************************************************************************************/
/* --------------------------------> PORT configuration <---------------------------*/
/************************************************************************************/

	DIO_Register_PORTD = (DIO_u8_PIN24_INIT_PORT_VAL << 0) || (DIO_u8_PIN25_INIT_PORT_VAL << 1) ||
						(DIO_u8_PIN26_INIT_PORT_VAL << 2) || (DIO_u8_PIN27_INIT_PORT_VAL << 3) ||
						(DIO_u8_PIN28_INIT_PORT_VAL << 4) || (DIO_u8_PIN29_INIT_PORT_VAL << 5) ||
						(DIO_u8_PIN30_INIT_PORT_VAL << 6) || (DIO_u8_PIN31_INIT_PORT_VAL << 7);

/************************************************************************************/
/* --------------------------------> PIN configuration <----------------------------*/
/************************************************************************************/

	DIO_Register_PIND = (DIO_u8_PIN24_INIT_PIN_VAL << 0) || (DIO_u8_PIN25_INIT_PIN_VAL << 1) ||
						(DIO_u8_PIN26_INIT_PIN_VAL << 2) || (DIO_u8_PIN27_INIT_PIN_VAL << 3) ||
						(DIO_u8_PIN28_INIT_PIN_VAL << 4) || (DIO_u8_PIN29_INIT_PIN_VAL << 5) ||
						(DIO_u8_PIN30_INIT_PIN_VAL << 6) || (DIO_u8_PIN31_INIT_PIN_VAL << 7);

/************************************************************************************/
}

/************************************************************************************/
/* Name: 1. DIO_u8SetPinDirection											     	*/
/* Description: Set pin direction													*/
/* Inputs:  -> Pin number (copy_u8PinNB) (ranges from 0 -> 31)						*/
/*          -> Direction (copy_u8Direction) (either 0 for input or 1 for output)	*/
/* Outputs: -> Error Status (local_u8Error), Expects either OK=1, or NOK=0	     	*/
/************************************************************************************/
u8 DIO_u8SetPinDirection(u8 copy_u8PinNB, u8 copy_u8Direction)
{
/*===========================>> Local variables <<==================================*/

	u8 local_u8Error  = STD_ERROR_OK; /* ----> This is used as a return value       */

	/* To abstract the pin number in case the user enters it more than 8 */
	copy_u8PinNB = copy_u8PinNB % 8;
	
	/* validates the input if it's more than maximum number of pins					*/
	if(copy_u8PinNB < DIO_MAXPINNB)
	{
		switch (copy_u8Direction)
		{
			case DIO_PIN_INPUT:
			/* In case the pin direction is input */
				if((copy_u8PinNB >= DIO_PIN0) && (copy_u8PinNB <= DIO_PIN7))
				{
					DIO_Register_DDRA &= ~(1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN8) && (copy_u8PinNB <= DIO_PIN15))
				{
					DIO_Register_DDRB &= ~(1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN16) && (copy_u8PinNB <= DIO_PIN23))
				{
					DIO_Register_DDRC &= ~(1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN24) && (copy_u8PinNB <= DIO_PIN31))
				{
					DIO_Register_DDRD &= ~(1 << copy_u8PinNB);
				}
				break;

			case DIO_PIN_OUTPUT:
			/* In case the pin direction is output */
				if((copy_u8PinNB >= DIO_PIN0) && (copy_u8PinNB <= DIO_PIN7))
				{
					DIO_Register_DDRA |= (1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN8) && (copy_u8PinNB <= DIO_PIN15))
				{
					DIO_Register_DDRB |= (1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN16) && (copy_u8PinNB <= DIO_PIN23))
				{
					DIO_Register_DDRC |= (1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN24) && (copy_u8PinNB <= DIO_PIN31))
				{
					DIO_Register_DDRD |= (1 << copy_u8PinNB);
				}
				break;

			default:
			/* Should enter this case if the input value is not input or output */
				local_u8Error = STD_ERROR_NOK;
				break;
		}
	}
	else
	{
		/* In case the number of pins are more than maximum number defined */
		local_u8Error = STD_ERROR_NOK;
	}

	return local_u8Error;
}

/************************************************************************************/
/* Name: 2. DIO_u8SetPinValue											         	*/
/* Description: Set pin value    													*/
/* Inputs:  -> Pin number (copy_u8PinNB) (ranges from 0 -> 31)						*/
/*          -> Value (copy_u8Value) (either 0 or 1)	                                */
/* Outputs: -> Error Status (local_u8Error), Expects either OK=1, or NOK=0	     	*/
/************************************************************************************/
u8 DIO_u8SetPinValue(u8 copy_u8PinNB, u8 copy_u8Value)
{
	/*Local variables*/
	u8 local_u8Error = STD_ERROR_OK;
	
	/* To abstract the pin number in case the user enters it more than 8 */
	copy_u8PinNB = copy_u8PinNB % 8;
	
	/* Validates the input to check whether the user enter the
	 * correct pin number, and whether he enters the right which
	 * is not bigger than upper limit.
	 */
	if(copy_u8PinNB < DIO_MAXPINNB)
	{
		switch(copy_u8Value)
		{
			case DIO_PIN_LOW:
			/* In case the pin value needed is low */
				if((copy_u8PinNB >= DIO_PIN0) && (copy_u8PinNB <= DIO_PIN7))
				{
					DIO_Register_PORTA &= ~(1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN8) && (copy_u8PinNB <= DIO_PIN15))
				{
					DIO_Register_PORTB &= ~(1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN16) && (copy_u8PinNB <= DIO_PIN23))
				{
					DIO_Register_PORTC &= ~(1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN24) && (copy_u8PinNB <= DIO_PIN31))
				{
					DIO_Register_PORTD &= ~(1 << copy_u8PinNB);
				}
				break;

			case DIO_PIN_HIGH:
			/* In case the pin value needed is high */
				if((copy_u8PinNB >= DIO_PIN0) && (copy_u8PinNB <= DIO_PIN7))
				{
					DIO_Register_PORTA |= (1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN8) && (copy_u8PinNB <= DIO_PIN15))
				{
					DIO_Register_PORTB |= (1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN16) && (copy_u8PinNB <= DIO_PIN23))
				{
					DIO_Register_PORTC |= (1 << copy_u8PinNB);
				}
				else if((copy_u8PinNB >= DIO_PIN24) && (copy_u8PinNB <= DIO_PIN31))
				{
					DIO_Register_PORTD |= (1 << copy_u8PinNB);
				}
				break;

			default:
			/* This should be entered if the value entered is not output or input */
				local_u8Error = STD_ERROR_NOK;
		}

	}
	else
	{
		/* Should be entered if the value of pin number is more than maximum */
		local_u8Error = STD_ERROR_NOK;
	}

	return local_u8Error;
}

/************************************************************************************/
/* Name: 3. DIO_u8ReadPinValue											         	*/
/* Description: Read pin value    													*/
/* Inputs:  -> Pin number (copy_u8PinNB) (ranges from 0 -> 31)						*/
/*          -> Location (copy_u8Value)                        						*/
/* Outputs: -> Error Status (local_u8Error), Expects either OK=1, or NOK=0	     	*/
/************************************************************************************/
u8 DIO_u8ReadPinValue(u8 copy_u8PinNB, u8 * copy_pu8Value)
{
	/*Local variables*/
	u8 local_u8Error = STD_ERROR_OK;
	
	/* To abstract the pin number in case the user enters it more than 8 */
	copy_u8PinNB = copy_u8PinNB % 8;
	
	if((copy_u8PinNB < DIO_MAXPINNB) && (copy_pu8Value != NULL))
	{
		if((copy_u8PinNB >= DIO_PIN0) && (copy_u8PinNB <= DIO_PIN7))
		{
			* copy_pu8Value = (DIO_Register_PINA) & (1 << copy_u8PinNB);
		}
		else if((copy_u8PinNB >= DIO_PIN8) && (copy_u8PinNB <= DIO_PIN15))
		{
			* copy_pu8Value = (DIO_Register_PINB) & (1 << copy_u8PinNB);
		}
		else if((copy_u8PinNB >= DIO_PIN16) && (copy_u8PinNB <= DIO_PIN23))
		{
			* copy_pu8Value = (DIO_Register_PINC) & (1 << copy_u8PinNB);
		}
		else if((copy_u8PinNB >= DIO_PIN24) && (copy_u8PinNB <= DIO_PIN31))
		{
			* copy_pu8Value = (DIO_Register_PIND) & (1 << copy_u8PinNB);
		}
		local_u8Error = STD_ERROR_OK;
	}
	else
	{
		/* This should be entered in case the pin number is more than maximum */
		/* or the container address is NULL 								  */
		local_u8Error = STD_ERROR_NOK;
	}

	return local_u8Error;
}


/************************************************************************************/
/*------------------------------>  PORT OPERATIONS   <------------------------------*/
/************************************************************************************/

/************************************************************************************/
/* Name: 1. DIO_u8SetPortDirethe 													*/
/* Description: Set Port value    													*/
/* Inputs:  -> Pin number (copy_u8PortNB) (ranges from 0 -> 3)						*/
/*          -> Location (copy_u8Value)                        						*/
/* Outputs: -> Error Status (local_u8Error), Expects either OK=1, or NOK=0	     	*/
/************************************************************************************/
u8 DIO_u8SetPortDirection(u8 copy_u8PortNB, u8 copy_u8Direction)
{
	/*Local variables*/
	u8 local_u8Error = STD_ERROR_OK;

	if(copy_u8PortNB < DIO_MAXPORTNB)
	{
		switch(copy_u8Direction)
		{
			case DIO_PORT_INPUT:
			/* If the port value needs to be input */
				if(copy_u8PortNB == DIO_PORTA)
				{
					DIO_Register_DDRA = 0x00;
				}
				else if(copy_u8PortNB == DIO_PORTB)
				{
					DIO_Register_DDRB = 0x00;
				}
				else if(copy_u8PortNB == DIO_PORTC)
				{
					DIO_Register_DDRC = 0x00;
				}
				else if(copy_u8PortNB == DIO_PORTD)
				{
					DIO_Register_DDRD = 0x00;
				}
				break;

			case DIO_PORT_OUTPUT:
			/* If the port value needs to be output */
				switch (copy_u8PortNB)
				{
				case DIO_PORTA:
					DIO_Register_DDRA = 0xFF;
					break;
				case DIO_PORTB:
					DIO_Register_DDRB = 0xFF;
					break;
				case DIO_PORTC:
					DIO_Register_DDRC = 0xFF;
					break;
				case DIO_PORTD:
					DIO_Register_DDRD = 0xFF;
					break;
				}
				break;
			default:
			/* In case the value of port is not input or output */
				local_u8Error = STD_ERROR_NOK;
				break;
		}
	}
	else
	{
		local_u8Error = STD_ERROR_NOK;
	}

	return local_u8Error;
}

/************************************************************************************/
/* Name: 2. DIO_u8SetPortValue											         	*/
/* Description: Set port value    													*/
/* Inputs:  -> Pin number (copy_u8PortNB) (ranges from 0 -> 3)						*/
/*          -> Value (copy_u8Value) (from 0x00 till 0xFF)                           */
/* Outputs: -> Error Status (local_u8Error), Expects either OK=1, or NOK=0	     	*/
/************************************************************************************/
u8 DIO_u8SetPortValue(u8 copy_u8PortNB, u8 copy_pu8Value)
{
	/*Local variables*/
	u8 local_u8Error = STD_ERROR_OK;

	if( copy_u8PortNB < DIO_MAXPORTNB )
	{
		switch(copy_pu8Value)
		{
			case DIO_PORT_LOW:
				if(copy_u8PortNB == DIO_PORTA)
				{
					DIO_Register_PORTA = 0x00;
				}
				else if(copy_u8PortNB == DIO_PORTB)
				{
					DIO_Register_PORTB = 0x00;
				}
				else if(copy_u8PortNB == DIO_PORTC)
				{
					DIO_Register_PORTC = 0x00;
				}
				else if(copy_u8PortNB == DIO_PORTD)
				{
					DIO_Register_PORTD = 0x00;
				}
				break;

			case DIO_PORT_OUTPUT:
				if(copy_u8PortNB == DIO_PORTA)
				{
					DIO_Register_PORTA = 0xFF;
				}
				else if(copy_u8PortNB == DIO_PORTB)
				{
					DIO_Register_PORTB = 0xFF;
				}
				else if(copy_u8PortNB == DIO_PORTC)
				{
					DIO_Register_PORTC = 0xFF;
				}
				else if(copy_u8PortNB == DIO_PORTD)
				{
					DIO_Register_PORTD = 0xFF;
				}
				break;

			case DIO_BYTE_VALUE_RANGE:
				if(copy_u8PortNB == DIO_PORTA)
				{
					DIO_Register_PORTA = copy_pu8Value;
				}
				else if(copy_u8PortNB == DIO_PORTB)
				{
					DIO_Register_PORTB = copy_pu8Value;
				}
				else if(copy_u8PortNB == DIO_PORTC)
				{
					DIO_Register_PORTC = copy_pu8Value;
				}
				else if(copy_u8PortNB == DIO_PORTD)
				{
					DIO_Register_PORTD = copy_pu8Value;
				}
				break;
			default:
				local_u8Error = STD_ERROR_NOK;
		}
	}
	else
	{
		local_u8Error = STD_ERROR_NOK;
	}

	return local_u8Error;
}


/************************************************************************************/
/* Name: 3. DIO_u8ReadPortValue											         	*/
/* Description: Read port value    													*/
/* Inputs:  -> Pin number (copy_u8PortNB) (ranges from 0 -> 3)						*/
/*          -> Location (* copy_pu8Value)                                           */
/* Outputs: -> Error Status (local_u8Error), Expects either OK=1, or NOK=0	     	*/
/************************************************************************************/
u8 DIO_u8ReadPortValue(u8 copy_u8PortNB, u8 * copy_pu8Value)
{
	/*Local variables*/
	u8 local_u8Error = STD_ERROR_OK;

	if((copy_u8PortNB < DIO_MAXPINNB) && (copy_pu8Value != NULL))
		{
			if(copy_u8PortNB == DIO_PORTA)
			{
				* copy_pu8Value = DIO_Register_PINA;
			}
			else if(copy_u8PortNB == DIO_PORTB)
			{
				* copy_pu8Value = DIO_Register_PINB;
			}
			else if(copy_u8PortNB == DIO_PORTC)
			{
				* copy_pu8Value = DIO_Register_PINC;
			}
			else if(copy_u8PortNB == DIO_PORTD)
			{
				* copy_pu8Value = DIO_Register_PIND;
			}
			local_u8Error = STD_ERROR_OK;
		}
	else
	{
		local_u8Error = STD_ERROR_NOK;
	}

	return local_u8Error;
}
