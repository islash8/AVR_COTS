## This is the readme file for the DIO driver for ATmega32 AVR MCU

### DIO Private:
-> The DIO private file is used to define private values used inside the driver itself.
1. Define the Maximum number of pins and ports for data validation
2. The values used for switch cases that will detect whether to assign input or output, high or low
3. Addresses of each pin and each port
-> For this to work, you need to include the STD_Types.h

### DIO Interface:
-> The DIO interface file is used to define the APIs and macro definitions of global values used by external files.
1. Define the Macros for values used by external files
2. Define names of ports and pins
3. Define Available APIs

### DIO Config:
-> The DIO config file contains the initial value used by the DIO Initialization function
1. Initiate the values for direction
2. Initiate the values for port register

### DIO Prog:
-> It contains the source code for the DIO driver
1. The initialization function just intializes the values of registers with configuration values
2. Set pin direction function is used to set or reset pin values in DDRx register, depending on the passing arguments
3. Set port value for pins, which change the values of PORTx register for each pin
4. Read pin value for pins, which reads the value of PINx register for each pin
5. Also same goes for whole port with set port value, read port value and set port direction